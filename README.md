Installation using Composer (recommended)
========================================

If you use Composer to manage dependencies, edit "/composer.json" as follows.

  1. Run "composer require --prefer-dist composer/installers" to ensure that you
     have the "composer/installers" package installed. This package facilitates
     the installation of packages into directories other than "/vendor" (e.g.
     "/libraries") using Composer.

  2. Add the following to the "installer-paths" section of "composer.json":

     "libraries/{$name}": ["type:drupal-library"],

  3. Add the following to the "repositories" section of "composer.json":

  ```
  {
      "type": "package",
      "package": {
      "name": "highstock/highstock",
      "version": "11.1.0",
      "type": "drupal-library",
      "extra": {
          "installer-name": "highstock"
      },
      "dist": {
          "url": "https://code.highcharts.com/stock/11.1.0/highstock.js",
          "type": "file"
      },
      "require": {
          "composer/installers": "~1.0"
      }
  },
  {
      "type": "package",
      "package": {
      "name": "highstock/data",
      "version": "11.1.0",
      "type": "drupal-library",
      "extra": {
          "installer-name": "highstock_data"
      },
      "dist": {
          "url": "https://code.highcharts.com/stock/11.1.0/data.js",
          "type": "file"
      },
      "require": {
          "composer/installers": "~1.0"
      }
  },
  {
      "type": "package",
      "package": {
      "name": "highstock/exporting",
      "version": "11.1.0",
      "type": "drupal-library",
      "extra": {
          "installer-name": "highstock_exporting"
      },
          "dist": {
          "url": "https://code.highcharts.com/stock/11.1.0/modules/exporting.js",
      "type": "file"
      },
      "require": {
          "composer/installers": "~1.0"
      }
  },
  {
      "type": "package",
      "package": {
      "name": "highstock/export-data",
      "version": "11.1.0",
      "type": "drupal-library",
      "extra": {
          "installer-name": "highstock_export-data"
      },
      "dist": {
          "url": "https://code.highcharts.com/stock/11.1.0/modules/export-data.js",
          "type": "file"
      },
      "require": {
          "composer/installers": "~1.0"
      }
  },
  {
      "type": "package",
      "package": {
      "name": "highstock/accessibility",
      "version": "11.1.0",
      "type": "drupal-library",
      "extra": {
          "installer-name": "highstock_accessibility"
      },
      "dist": {
          "url": "https://code.highcharts.com/11.1.0/modules/accessibility.js",
          "type": "file"
      },
      "require": {
          "composer/installers": "~1.0"
      }
  },
  {
      "type": "package",
      "package": {
      "name": "highstock/no_data",
      "version": "11.1.0",
      "type": "drupal-library",
      "extra": {
          "installer-name": "highstock_no_data"
      },
      "dist": {
          "url": "https://code.highcharts.com/11.1.0/modules/no-data-to-display.js",
          "type": "file"
      },
      "require": {
          "composer/installers": "~1.0"
      }
  },
  {
      "type": "package",
      "package": {
      "name": "highcharts/more",
      "version": "11.1.0",
      "type": "drupal-library",
      "extra": {
          "installer-name": "highcharts_more"
      },
      "dist": {
          "url": "https://code.highcharts.com/11.1.0/highcharts-more.js",
          "type": "file"
      },
      "require": {
          "composer/installers": "~1.0"
      }
  }
  ```

4. Run "composer require --prefer-dist highstock/highstock:11.1.0 highstock/more:11.1.0 highstock/exporting:11.1.0 highstock/export-data:11.1.0 highstock/accessibility:11.1.0 highstock/3d:11.1.0"
- you should find that new directories have been created under "/libraries"
