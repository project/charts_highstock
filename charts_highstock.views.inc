<?php

/**
 * @file
 * Implements hook_views_data().
 */

/**
 * Implements hook_views_data().
 */
function charts_highstock_views_data() {
  $data['charts_highstock']['table']['group'] = t('Global');
  $data['charts_highstock']['table']['join'] = [
    // Exist in all views.
    '#global' => [],
  ];
  $data['charts_highstock']['field_charts_highstock'] = [
    'title' => t('Highstock Value Field'),
    'help' => t('Views field that returns the timestamp and value for Highstock.'),
    'field' => [
      'id' => 'field_charts_highstock',
    ],
  ];

  return $data;
}
