<?php

namespace Drupal\charts_highstock\Plugin\chart\Library;

use Drupal\charts_highcharts\Plugin\chart\Library\Highcharts;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a concrete class for a Highstock.
 *
 * @Chart(
 *   id = "highstock",
 *   name = @Translation("Highstock")
 * )
 */
class Highstock extends Highcharts {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $configurations = parent::defaultConfiguration();
    $configurations['global_options']['lang']['range_selector_zoom'] = 'Zoom';
    return $configurations;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    // Remove unneeded intro text.
    unset($form['intro_text']);

    $lang_config = $this->defaultConfiguration()['global_options']['lang'];
    $form['global_options']['lang']['range_selector_zoom'] = [
      '#title' => $this->t('Range selector zoom'),
      '#type' => 'textfield',
      '#description' => $this->t('The text for the label for the range selector buttons.'),
      '#default_value' => $this->configuration['global_options']['lang']['range_selector_zoom'] ?? $lang_config['range_selector_zoom'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function preRender(array $element) {
    $element = parent::preRender($element);

    if (!isset($element['#id'])) {
      $element['#id'] = Html::getUniqueId('highstock-render');
    }

    // Removing library added by the parent highcharts library.
    $element['#attached']['library'] = [
      'charts_highstock/default',
      'charts_highstock/highstock',
    ];

    // Add the highstock global options.
    $element['#attached']['drupalSettings']['charts']['highstock'] = $element['#attached']['drupalSettings']['charts']['highcharts'];
    unset($element['#attached']['drupalSettings']['charts']['highcharts']);

    // Remove the charts-highcharts class.
    $css_class = 'charts-highchart';
    $css_class_index = array_search($css_class, $element['#attributes']['class']);
    unset($element['#attributes']['class'][$css_class_index]);
    $element['#attributes']['class'][] = 'charts-highstock';

    // Resetting the color changing for now.
    $element['#content_suffix'] = [];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  protected function populateOptions(array $element, array $chart_definition) {
    $chart_definition = parent::populateOptions($element, $chart_definition);

    $chart_definition['chart']['width'] = $element['#width'] ?? NULL;
    $chart_definition['chart']['height'] = $element['#height'] ?? NULL;

    $chart_definition['rangeSelector']['buttons'] = [
      [
        'type' => 'month',
        'count' => 1,
        'text' => $this->t('1m'),
        'title' => $this->t('View 1 month'),
      ],
      [
        'type' => 'month',
        'count' => 3,
        'text' => $this->t('3m'),
        'title' => $this->t('View 3 months'),
      ],
      [
        'type' => 'month',
        'count' => 6,
        'text' => $this->t('6m'),
        'title' => $this->t('View 6 months'),
      ],
      [
        'type' => 'ytd',
        'text' => $this->t('YTD'),
        'title' => $this->t('View year to date'),
      ],
      [
        'type' => 'year',
        'count' => 1,
        'text' => $this->t('1y'),
        'title' => $this->t('View 1 year'),
      ],
      [
        'type' => 'all',
        'text' => $this->t('All'),
        'title' => $this->t('View all'),
      ],
    ];

    // Merge in chart raw options.
    if (!empty($element['#raw_options'])) {
      $chart_definition = NestedArray::mergeDeepArray([
        $chart_definition,
        $element['#raw_options'],
      ]);
    }

    return $chart_definition;
  }

  /**
   * {@inheritdoc}
   */
  protected function populateData(array &$element, array $chart_definition) {
    return parent::populateData($element, $chart_definition);
  }

  /**
   * {@inheritdoc}
   */
  protected function populateAxes(array $element, array $chart_definition) {
    return parent::populateAxes($element, $chart_definition);
  }

}
