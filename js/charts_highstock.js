/**
 * @file
 * JavaScript integration between Highstock and Drupal.
 */
(function (Drupal, once, drupalSettings) {

  'use strict';

  Drupal.chartsHighstock = Drupal.chartsHighstock || {};
  // Property to keep track of enabled series data label per chart.
  Drupal.chartsHighstock.chartDisplayDataLabelsEnabled = [];
  // e: PointerEvent
  Drupal.chartsHighstock.menuItemToggleDataLabelClickCallback = function (e) {
    const chart = this,
      chartId = chart.options.drupalChartDivId;
    Drupal.chartsHighstock.chartDisplayDataLabelsEnabled[chartId] = !Drupal.chartsHighstock.chartDisplayDataLabelsEnabled[chartId];
    chart.series.forEach(
      (item, i) => {
        chart.series[i].update({
          dataLabels: {
            enabled: Drupal.chartsHighstock.chartDisplayDataLabelsEnabled[chartId],
          },
        });
      }
    );
  };

  Drupal.behaviors.chartsHighstock = {
    attach: function (context, settings) {
      const contents = new Drupal.Charts.Contents();

      if (drupalSettings.hasOwnProperty('charts') && drupalSettings.charts.highstock.global_options) {
        Highcharts.setOptions(drupalSettings.charts.highstock.global_options);
      }
      once('charts-highstock', '.charts-highstock', context).forEach(function (element) {
        const config = contents.getData(element.id);
        if (element.nextElementSibling && element.nextElementSibling.hasAttribute('data-charts-debug-container')) {
          element.nextElementSibling.querySelector('code').innerText = JSON.stringify(config, null, ' ');
        }
        if (config) {
          config.exporting = config.exporting || {
            menuItemDefinitions: {
              toggleDataLabel: {
                onclick: Drupal.chartsHighstock.menuItemToggleDataLabelClickCallback,
                text: Drupal.t('Toggle data label display'),
              },
            },
            buttons: {
              contextButton: {
                menuItems: [
                  'viewFullscreen',
                  'printChart',
                  'separator',
                  'downloadPNG',
                  'downloadPDF',
                  'downloadSVG',
                  'separator',
                  'downloadCSV',
                  'viewData',
                  'toggleDataLabel'
                ],
              },
            },
          };
          Drupal.chartsHighstock.chartDisplayDataLabelsEnabled[element.id] = config.plotOptions.series.dataLabels.enabled;
          Highcharts.stockChart(element.id, config);
        }
      });
    },
    detach: function (context, settings, trigger) {
      if (trigger === 'unload') {
        once('charts-highstock-detach', '.charts-highstock', context).forEach(function (element) {
          const highstock_item =  Highcharts.stockChart(element.id);
          if (highstock_item) {
            highstock_item.destroy();
          }
        });
      }
    }

  };
}(Drupal, once, drupalSettings));
